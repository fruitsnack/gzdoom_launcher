# gzdoom_launcher

A helper script to easily manage WADs and mods and launch GZDoom.

## Requirements

- gzdoom

## Usage

Consult script's --help output for usage.  
Some map packs come as WADs and require original DOOM2.WAD, in that case they
can be loaded as additional mods according to script arguments.

Example:  
`gzdoom_launcher -c pandemonia -w ~/Games/Doom/WADs -m 
~/Games/Doom/Mods -i DOOM2.WAD HR2.WAD`

Note: only a few arguments are required.

## License
This script is licensed under coffeeware (fork of beerware) license.
